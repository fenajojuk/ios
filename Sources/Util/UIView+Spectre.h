//
// Created by Maarten Billemont on 2020-06-04.
// Copyright (c) 2020 Lyndir. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView(Spectre)

@property(nonatomic) UIEdgeInsets alignmentRectInsets;
@property(nonatomic) UIEdgeInsets alignmentRectOutsets;
@property(nonatomic, readonly) CGRect alignmentRect;

@end
