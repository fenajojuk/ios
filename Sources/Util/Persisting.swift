//
// Created by Maarten Billemont on 2019-10-11.
// Copyright (c) 2019 Lyndir. All rights reserved.
//

import Foundation

protocol Persisting {
    var dirty: Bool { get set }
}

extension Persisting {
}
